package apmb

import (
	"encoding/binary"
	"time"

	"github.com/goburrow/modbus"
	"golang.org/x/text/encoding"
)

type Endianness string

const (
	BigEndian    Endianness = "BIG"
	LittleEndian Endianness = "LITTLE"
)

type Slave struct {
	Endianness      Endianness
	byteOrder       binary.ByteOrder
	byteOrderAppend binary.AppendByteOrder
}

type MBConnection struct {
	Encoding   encoding.Encoding
	Master     modbus.Client
	handler    *modbus.TCPClientHandler
	initParams initParams
}

type initParams struct {
	addr          string
	slaveId       byte
	timeOut, idle time.Duration
}

type Registry struct {
	RegAddress uint16 `json:"Address,omitempty"`
	RegNumber  uint16 `json:"Number,omitempty"`
}

type MbDatumUint16 struct {
	Registry    `json:"Registry"`
	Value       uint16 `json:"omitempty"`
	RevertOrder bool   `json:"RevertOrder"`
}

type MbDatumBool struct {
	Registry `json:"Registry"`
	Value    bool `json:"omitempty"`
}

type MbDatumInt16 struct {
	Registry    `json:"Registry"`
	Value       int16 `json:"omitempty"`
	RevertOrder bool  `json:"RevertOrder"`
}

type MbDatumUint32 struct {
	Registry    `json:"Registry"`
	Value       uint32 `json:"omitempty"`
	RevertOrder bool   `json:"RevertOrder"`
}

type MbDatumInt32 struct {
	Registry    `json:"Registry"`
	Value       int32 `json:"omitempty"`
	RevertOrder bool  `json:"RevertOrder"`
}

type MbDatumFloat32 struct {
	Registry    `json:"Registry"`
	Value       float32 `json:"omitempty"`
	RevertOrder bool    `json:"RevertOrder"`
}

type MbDatumString struct {
	Registry    `json:"Registry"`
	Value       string `json:"omitempty"`
	RevertOrder bool   `json:"RevertOrder"`
}

type Encoding struct {
	Name string `json:"Name"`
	Type encoding.Encoding
}

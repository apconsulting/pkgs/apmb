package apmb

import (
	"reflect"
	"time"

	"github.com/goburrow/modbus"
	"golang.org/x/text/encoding"
	"golang.org/x/text/encoding/charmap"
)

func NewConnection(addr string, slaveId byte, endianness string, enc string, timeout, idle time.Duration) MBConnection {
	handler := modbus.NewTCPClientHandler(addr)
	handler.SlaveId = slaveId
	handler.Timeout = timeout
	handler.IdleTimeout = idle

	return MBConnection{
		Encoding: getEncodingType(enc),
		Master:   modbus.NewClient(handler),
		handler:  handler,
		initParams: initParams{
			addr:    addr,
			slaveId: slaveId,
			timeOut: timeout,
			idle:    idle,
		}}
}

func getEncodingType(name string) encoding.Encoding {
	charList := charmap.All
	var charMap *charmap.Charmap
	for _, enc := range charList {
		char := enc.(any)
		voChar := reflect.ValueOf(char)
		cmType := reflect.TypeOf(charMap)
		if voChar.CanConvert(cmType) {
			charMap = char.(*charmap.Charmap)
			cmName := charMap.String()
			if cmName == name {
				return enc
			}
		}
	}
	return nil
}

func (mb *MBConnection) RefreshConnection() error {

	if err := mb.handler.Close(); err != nil {
		return err
	}

	handler := modbus.NewTCPClientHandler(mb.initParams.addr)
	handler.SlaveId = mb.initParams.slaveId
	handler.Timeout = mb.initParams.timeOut
	handler.IdleTimeout = mb.initParams.idle

	mb = &MBConnection{
		Master:     modbus.NewClient(handler),
		Encoding:   mb.Encoding,
		handler:    handler,
		initParams: mb.initParams,
	}

	return nil
}

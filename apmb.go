package apmb

func (mb MBConnection) ReadUint16(data MbDatumUint16) (uint16, error) {
	result, err := mb.Master.ReadHoldingRegisters(data.RegAddress, data.RegNumber)
	if err != nil {
		return 0, err
	}
	return readUint16(result, data.RevertOrder), nil
}

func (mb MBConnection) ReadInt16(data MbDatumInt16) (int16, error) {
	result, err := mb.Master.ReadHoldingRegisters(data.RegAddress, data.RegNumber)
	if err != nil {
		return 0, err
	}
	return readInt16(result, data.RevertOrder)
}

func (mb MBConnection) ReadUint32(data MbDatumUint32) (uint32, error) {
	result, err := mb.Master.ReadHoldingRegisters(data.RegAddress, data.RegNumber)
	if err != nil {
		return 0, err
	}
	return readUint32(result, data.RevertOrder), nil
}

func (mb MBConnection) ReadInt32(data MbDatumInt32) (int32, error) {
	result, err := mb.Master.ReadHoldingRegisters(data.RegAddress, data.RegNumber)
	if err != nil {
		return 0, err
	}
	return readInt32(result, data.RevertOrder)
}

func (mb MBConnection) ReadFloat32(data MbDatumFloat32) (float32, error) {
	result, err := mb.Master.ReadHoldingRegisters(data.RegAddress, data.RegNumber)
	if err != nil {
		return 0, err
	}
	return readFloat32(result, data.RevertOrder)
}
func (mb MBConnection) ReadString(data MbDatumString) (string, error) {
	result, err := mb.Master.ReadHoldingRegisters(data.RegAddress, data.RegNumber)
	if err != nil {
		return "", err
	}

	if mb.Encoding != nil {
		bts, err := mb.Encoding.NewDecoder().Bytes(result)
		if err != nil {
			return "", err
		}
		result = bts
	}

	return cleanString(readString(result, data.RevertOrder)), nil
}

func (mb MBConnection) WriteUint16(data MbDatumUint16) error {
	dataBytes, err := prepareUint16(data.Value, data.RevertOrder)
	if err != nil {
		return err
	}

	if _, err := mb.Master.WriteMultipleRegisters(data.RegAddress, data.RegNumber, dataBytes); err != nil {
		return err
	}

	return nil
}

func (mb MBConnection) WriteUint32(data MbDatumUint32) error {
	dataBytes, err := prepareUint32(data.Value, data.RevertOrder)
	if err != nil {
		return err
	}

	if _, err := mb.Master.WriteMultipleRegisters(data.RegAddress, data.RegNumber, dataBytes); err != nil {
		return err
	}

	return nil
}

func (mb MBConnection) WriteInt32(data MbDatumInt32) error {
	dataBytes, err := prepareInt32(data.Value, data.RevertOrder)
	if err != nil {
		return err
	}

	if _, err := mb.Master.WriteMultipleRegisters(data.RegAddress, data.RegNumber, dataBytes); err != nil {
		return err
	}

	return nil
}

func (mb MBConnection) WriteFloat32(data MbDatumFloat32) error {
	dataBytes, err := prepareFloat32(data.Value, data.RevertOrder)
	if err != nil {
		return err
	}

	if _, err := mb.Master.WriteMultipleRegisters(data.RegAddress, data.RegNumber, dataBytes); err != nil {
		return err
	}

	return nil
}

func (mb MBConnection) WriteString(data MbDatumString) error {

	padded := padStr(data.Value, int(data.RegNumber))
	dataBytes := prepareString(padded, data.RevertOrder)

	if mb.Encoding != nil {
		bts, err := mb.Encoding.NewEncoder().Bytes(dataBytes)
		if err != nil {
			return err
		}
		dataBytes = bts
	}

	if _, err := mb.Master.WriteMultipleRegisters(data.RegAddress, data.RegNumber, dataBytes); err != nil {
		return err
	}

	return nil
}

func (mb MBConnection) ReadUint16Input(data MbDatumUint16) (uint16, error) {
	result, err := mb.Master.ReadDiscreteInputs(data.RegAddress, data.RegNumber)
	if err != nil {
		return 0, err
	}

	return readUint16(result, data.RevertOrder), nil
}
func (mb MBConnection) ReadInt16Input(data MbDatumInt16) (int16, error) {
	result, err := mb.Master.ReadDiscreteInputs(data.RegAddress, data.RegNumber)
	if err != nil {
		return 0, err
	}

	return readInt16(result, data.RevertOrder)
}

func (mb MBConnection) ReadUint32Input(data MbDatumUint32) (uint32, error) {
	result, err := mb.Master.ReadDiscreteInputs(data.RegAddress, data.RegNumber)
	if err != nil {
		return 0, err
	}

	return readUint32(result, data.RevertOrder), nil
}

func (mb MBConnection) ReadInt32Input(data MbDatumInt32) (int32, error) {
	result, err := mb.Master.ReadDiscreteInputs(data.RegAddress, data.RegNumber)
	if err != nil {
		return 0, err
	}

	return readInt32(result, data.RevertOrder)
}

func (mb MBConnection) ReadFloat32Input(data MbDatumFloat32) (float32, error) {
	result, err := mb.Master.ReadDiscreteInputs(data.RegAddress, data.RegNumber)
	if err != nil {
		return 0, err
	}

	return readFloat32(result, data.RevertOrder)
}

func (mb MBConnection) ReadStringInput(data MbDatumString) (string, error) {
	result, err := mb.Master.ReadDiscreteInputs(data.RegAddress, data.RegNumber)
	if err != nil {
		return "", err
	}

	return readString(result, data.RevertOrder), nil
}

func (mb MBConnection) ReadCoils(data MbDatumBool) (bool, error) {
	r, err := mb.Master.ReadCoils(data.RegAddress, data.RegNumber)
	if err != nil {
		return false, err
	}
	return boolFromBytes(r), nil
}

func (mb MBConnection) WriteSingleCoil(data MbDatumBool) error {
	b := boolToUint16(data.Value)
	_, err := mb.Master.WriteSingleCoil(data.RegAddress, b)
	if err != nil {
		return err
	}
	return nil
}

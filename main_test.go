package apmb

import (
	"fmt"
	"testing"
	"time"
)

type testData struct {
	str MbDatumString
	u16 MbDatumUint16
	u32 MbDatumUint32
	f32 MbDatumFloat32
}

func TestEndianness(t *testing.T) {
	myData := testData{
		MbDatumString{
			Value:       "1a2b3c",
			RevertOrder: true,
			Registry: Registry{
				RegAddress: 0,
				RegNumber:  5,
			}},
		MbDatumUint16{
			Value:       252,
			RevertOrder: true,
			Registry: Registry{
				RegAddress: 10,
				RegNumber:  2,
			}},
		MbDatumUint32{
			Value:       252,
			RevertOrder: true,
			Registry: Registry{
				RegAddress: 20,
				RegNumber:  2,
			}},
		MbDatumFloat32{
			Value:       3.14,
			RevertOrder: true,
			Registry: Registry{
				RegAddress: 30,
				RegNumber:  2,
			}},
	}

	mbClient := NewConnection("127.0.0.1:502", 1, "BIG", "", 10*time.Second, 3*time.Minute)

	// WRITE OPS
	mbClient.WriteString(myData.str)
	mbClient.WriteUint16(myData.u16)
	mbClient.WriteUint32(myData.u32)
	mbClient.WriteFloat32(myData.f32)

	// READ OPS
	s, _ := mbClient.ReadString(myData.str)
	u16, _ := mbClient.ReadUint16(myData.u16)
	u32, _ := mbClient.ReadUint32(myData.u32)
	f32, _ := mbClient.ReadFloat32(myData.f32)

	fmt.Printf("String: %s \n Uint16: %v \n Uint32: %v \n Float32: %v", s, u16, u32, f32)

}

module gitlab.com/apconsulting/pkgs/apmb

go 1.20

require (
	github.com/goburrow/modbus v0.1.0
	golang.org/x/text v0.10.0
)

require github.com/goburrow/serial v0.1.0 // indirect

package apmb

import (
	"bytes"
	"encoding/binary"
	"slices"
	"strings"
)

// func readBool(value []byte, revertOrder bool) (bool, error) {
// 	buf := bytes.NewBuffer(value)
// 	var data bool
// 	if revertOrder {
// 		return data, binary.Read(buf, binary.LittleEndian, &data)
// 	}
// 	return data, binary.Read(buf, binary.BigEndian, &data)
// }

func readUint16(value []byte, revertOrder bool) uint16 {
	if revertOrder {
		return binary.LittleEndian.Uint16(value)
	}
	return binary.BigEndian.Uint16(value)
}

func readUint32(value []byte, revertOrder bool) uint32 {
	if revertOrder {
		return binary.LittleEndian.Uint32(value)
	}
	return binary.BigEndian.Uint32(value)
}

func readInt16(value []byte, revertOrder bool) (int16, error) {
	buf := bytes.NewBuffer(value)
	var data int16
	if revertOrder {
		return data, binary.Read(buf, binary.LittleEndian, &data)
	}
	return data, binary.Read(buf, binary.BigEndian, &data)
}

func readInt32(value []byte, revertOrder bool) (int32, error) {
	buf := bytes.NewBuffer(value)
	var data int32
	if revertOrder {
		return data, binary.Read(buf, binary.LittleEndian, &data)
	}
	return data, binary.Read(buf, binary.BigEndian, &data)
}

func readFloat32(value []byte, revertOrder bool) (float32, error) {
	buf := bytes.NewBuffer(value)
	var data float32
	if revertOrder {
		return data, binary.Read(buf, binary.LittleEndian, &data)
	}
	return data, binary.Read(buf, binary.BigEndian, &data)
}

func readString(value []byte, revertOrder bool) string {
	if revertOrder {
		sb := value
		b := make([]byte, len(sb)+(len(sb)%2))
		copy(b, sb)
		for i := 0; i < len(b); i += 2 {
			b[i], b[i+1] = b[i+1], b[i]
		}
		return string(b)
	}
	return string(value)
}

func prepareUint16(data uint16, revertOrder bool) ([]byte, error) {
	buf := bytes.NewBuffer([]byte{})
	if revertOrder {
		if err := binary.Write(buf, binary.LittleEndian, data); err != nil {
			return nil, err
		}
		return buf.Bytes(), nil
	}
	if err := binary.Write(buf, binary.BigEndian, data); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

// func prepareInt16(data int16, revertOrder bool) ([]byte, error) {
// 	buf := bytes.NewBuffer([]byte{})
// 	if revertOrder {
// 		if err := binary.Write(buf, binary.LittleEndian, data); err != nil {
// 			return nil, err
// 		}
// 		return buf.Bytes(), nil
// 	}
// 	if err := binary.Write(buf, binary.BigEndian, data); err != nil {
// 		return nil, err
// 	}
// 	return buf.Bytes(), nil
// }

func prepareUint32(data uint32, revertOrder bool) ([]byte, error) {
	buf := bytes.NewBuffer([]byte{})
	if revertOrder {
		if err := binary.Write(buf, binary.LittleEndian, data); err != nil {
			return nil, err
		}
		return buf.Bytes(), nil
	}
	if err := binary.Write(buf, binary.BigEndian, data); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func prepareInt32(data int32, revertOrder bool) ([]byte, error) {
	buf := bytes.NewBuffer([]byte{})
	if revertOrder {
		if err := binary.Write(buf, binary.LittleEndian, data); err != nil {
			return nil, err
		}
		return buf.Bytes(), nil
	}
	if err := binary.Write(buf, binary.BigEndian, data); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func prepareFloat32(data float32, revertOrder bool) ([]byte, error) {
	buf := bytes.NewBuffer([]byte{})
	if revertOrder {
		if err := binary.Write(buf, binary.LittleEndian, data); err != nil {
			return nil, err
		}
		return buf.Bytes(), nil
	}
	if err := binary.Write(buf, binary.BigEndian, data); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func prepareString(data string, revertOrder bool) []byte {
	if revertOrder {
		sb := []byte(data)
		b := make([]byte, len(sb)+(len(sb)%2))
		copy(b, sb)
		for i := 0; i < len(b); i += 2 {
			b[i], b[i+1] = b[i+1], b[i]
		}
		return b
	}
	return []byte(data)
}

// func prepareBool(data bool, revertOrder bool) ([]byte, error) {
// 	buf := bytes.NewBuffer([]byte{})
// 	if revertOrder {
// 		if err := binary.Write(buf, binary.LittleEndian, data); err != nil {
// 			return nil, err
// 		}
// 		return buf.Bytes(), nil
// 	}
// 	if err := binary.Write(buf, binary.BigEndian, data); err != nil {
// 		return nil, err
// 	}
// 	return buf.Bytes(), nil
// }

func padStr(s string, nRegs int) string {
	pad := strings.Repeat("\000", nRegs*2-len(s))
	return s + pad
}

func cleanString(stringa string) string {
	cleanString := ""
	for i := range stringa {
		if s := string(stringa[i]); s != "\000" {
			cleanString += s
		}
	}
	return cleanString
}

func boolFromBytes(b []byte) bool {
	return slices.Contains(b, 1)
}

func boolToUint16(b bool) uint16 {
	if b {
		return 0xFF00
	}
	return 0x0000
}
